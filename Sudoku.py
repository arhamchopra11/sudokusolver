from Numberjack import Model, Matrix, AllDiff
from math import floor
import collections

Point = collections.namedtuple("Point",['row','col', 'val'])

def getSudoku():
    print("Enter Your Sudoku: ")
    print("Enter each row on a new line, for each cell enter 0 if unknown or the corresponding digit")
    given_points = []
    for i in range(9):
        inp =str(input())
        inp = inp.split()
        try:
            point_array = [Point(row=i,col=j,val=int(inp[j])) for j in range(len(inp)) if int(inp[j]) is not 0  ]
        except:
            print("Incorrect input!!")

        given_points += point_array

    return given_points

def model_data(input_data):
    gameboard = Matrix(9,9,1,9)
    #  print(gameboard)
    model = Model(
            [AllDiff(row) for row in gameboard.row],
            [AllDiff(col) for col in gameboard.col],
            [AllDiff([gameboard[i+3*floor(k/3), j+3*(k%3)] for i in range(0,3) for j in range(0,3)]) for k in range(0,9)]
            )
    for point in input_data:
        model.add(gameboard[point.row, point.col]==point.val)
    #  print(gameboard)
    #  print(model)
    
    return (model, gameboard)

def solveSudoku():
    input_data = getSudoku()
    (model, variables) = model_data(input_data)
    solver = model.load('Mistral')
    solver.solve()
    #  print(variables)
    printSudoku(variables)

def printSudoku(results):
    for row in results:
        strout=""
        for i in range(0,9):
        #  print("|".join(v for v in row))
            if row[i].get_value() is None :
                print("Oops.... This Sudoku cannot be solved.... :( ")
                return
            strout += str(row[i].get_value()) +" "
        print(strout)
